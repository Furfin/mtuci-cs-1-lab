annual_salary = float(input("enter your salary:"))
portion_saved = float(input("enter percent of your salary saved as a decimal:"))
total_cost = float(input("enter total cost:"))
semi_annual_rise = float(input("enter semi annual rise as decimal:"))
portion_down_payment = 0.25
current_savings = 0
r = 0.04
monthly_salary = annual_salary/12
month = 0


while current_savings < total_cost*portion_down_payment:

    current_savings = current_savings + current_savings*(r/12)
    current_savings += monthly_salary*(portion_saved)
    month+=1
    if (month)%6 == 0:
        annual_salary = annual_salary + annual_salary*semi_annual_rise
        monthly_salary = annual_salary/12
print("Number of months:",month)



