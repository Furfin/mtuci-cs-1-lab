import sys



total_cost = 1000000
global_annual_salary = float(input("enter annual salary:"))

semi_annual_rise = 0.07
portion_down_payment = 0.25
global_current_savings = 0
r = 0.04
global_monthly_salary = global_annual_salary/12

i = 5000
bis = 0


while True:
    
    annual_salary = global_annual_salary
    current_savings = global_current_savings
    monthly_salary = global_monthly_salary
    month = 0
    while current_savings < (total_cost*0.25):
        current_savings = current_savings + current_savings*(r/12)
        current_savings += monthly_salary*(i/10000)
        month+=1
        if (month)%6 == 0:
                annual_salary = annual_salary + annual_salary*semi_annual_rise
                monthly_salary = annual_salary/12
    if month == 36:
        if round(i/10000,4) < 1:
            print("Best saving rate ",round(i/10000,4))
            print("Steps in bisection search ",bis)
        else:
            print("It is not possible to pay the cost in 3 years")
        break
    elif month > 36:
        i = i*1.5
        bis+=1
    elif month < 36:
        i = i*0.5
        bis+=1




